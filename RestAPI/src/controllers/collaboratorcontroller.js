const { Pool } = require('pg');

const pool = new Pool({
    host: 'localhost',
    user: '',
    password: '',
    database: '',
    port: '5432'
});

/*Method for get collaborators in data base */
const getCollaborator = async (req, res) => {
    const response = await pool.query('SELECT * from collaborators');
    res.status(200).json(response.rows);
}



module.exports = {
    getCollaborator
}