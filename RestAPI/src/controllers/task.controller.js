const { Pool } = require('pg');

const pool = new Pool({
    host: 'localhost',
    user: '',
    password: '',
    database: '',
    port: '5432'
});

/*Method to get Tasks in data base */
const getTask = async (req, res) => {
    try {
        const response = await pool.query('SELECT t.id, t.description, t.id_collaborator, c.name AS collaborator, t.state, t.priority, t.start_date, t.final_date, t.notes FROM tasks t LEFT JOIN collaborators c ON t.id_collaborator = c.id order by t.start_date ASC;');
        res.status(200).json(response.rows);
    } catch (error) {
        res.status(500).json({
            success: false,
            error: {
                code: 500,
                message: error.message
            }
        });
    }
};

/*Method to create Tasks in data base */
const createTask = async (req, res) => {
    const { description, id_collaborator, state, priority, start_date, final_date, notes } = req.body;
    try {
        const response = await pool.query('INSERT INTO tasks (description, id_collaborator, state, priority, start_date, final_date, notes) VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING *',
            [description, id_collaborator, state, priority, start_date, final_date, notes]);

        res.status(201).json({
            success: true,
            data: response.rows[0],
            message: 'Task Created'
        });
    } catch (error) {
        res.status(500).json({
            success: false,
            error: {
                code: 500,
                message: error.message
            }
        });
    }
};

/*Method to get by id Tasks in data base */
const getTaskId = async (req, res) => {
    const id = req.params.id;
    try {
        const response = await pool.query('SELECT t.id, t.description, c.name AS collaborator, t.state, t.priority, t.start_date, t.final_date, t.notes FROM tasks t LEFT JOIN collaborators c ON t.id_collaborator = c.id WHERE t.id = $1', [id]);

        if (response.rows.length === 0) {
            return res.status(404).json({
                success: false,
                error: {
                    code: 404,
                    message: 'Task not found'
                }
            });
        }

        res.status(200).json({
            success: true,
            data: response.rows[0]
        });
    } catch (error) {
        res.status(500).json({
            success: false,
            error: {
                code: 500,
                message: error.message
            }
        });
    }
};

/*Method to delete Tasks in data base*/
const deleteTask = async (req, res) => {
    const id = req.params.id;
    try {
        const response = await pool.query('DELETE FROM tasks WHERE id = $1', [id]);

        if (response.rowCount === 0) {
            return res.status(404).json({
                success: false,
                error: {
                    code: 404,
                    message: 'Task not found'
                }
            });
        }

        res.status(200).json({
            success: true,
            message: `Task ${id} deleted successfully`
        });
    } catch (error) {
        res.status(500).json({
            success: false,
            error: {
                code: 500,
                message: error.message
            }
        });
    }
};

/*Method to update Tasks in data base */
const updateTask = async (req, res) => {
    const id = req.params.id;
    const { description, id_collaborator, state, priority, start_date, final_date, notes } = req.body;

    try {
        const response = await pool.query(
            'UPDATE tasks SET description = $1, id_collaborator = $2, state = $3, priority = $4, start_date = $5, final_date = $6, notes = $7 WHERE id = $8',
            [description, id_collaborator, state, priority, start_date, final_date, notes, id]
        );

        if (response.rowCount === 0) {
            return res.status(404).json({
                success: false,
                error: {
                    code: 404,
                    message: 'Task not found'
                }
            });
        }

        res.status(200).json({
            success: true,
            message: 'Task updated successfully'
        });
    } catch (error) {
        res.status(500).json({
            success: false,
            error: {
                code: 500,
                message: error.message
            }
        });
    }
};

/*Method to filter tasks in data base */
const getFilteredTasks = async (req, res) => {
  
    try {
        const { id_collaborator, state, priority, start_date, final_date } = req.body;

        console.log("params:"+req.body.id_collaborator);

        let conditions = [];
        let values = [];

        if (id_collaborator!== null && id_collaborator !== 'All') {
            conditions.push(`id_collaborator = '${id_collaborator}'`);
        }
        if (state) {
            conditions.push(`state = '${state}'`);

        }
        if (priority) {
            conditions.push(`priority = '${priority}'`);

        }
        if (start_date && final_date) {
            conditions.push(`start_date >= '${start_date}' AND final_date <= '${final_date}'`);
        }

        let sqlQuery = 'SELECT t.id, t.description, c.name AS collaborator, t.state, t.priority, t.start_date, t.final_date, t.notes FROM tasks t LEFT JOIN collaborators c ON t.id_collaborator = c.id';

        if (conditions.length > 0) {
            sqlQuery += ' WHERE ' + conditions.join(' and ');
        }

        console.log('SQL Query:', sqlQuery);

        const response = await pool.query(sqlQuery);
        res.status(200).json(response.rows);
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

module.exports = {
    getTask,
    getTaskId,
    getFilteredTasks,
    createTask,
    deleteTask,
    updateTask
}