const express = require('express');
const app = express();
const cors = require('cors')

//middlewares
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cors());

//routes
app.use(require('./routes/Collaborator'));
app.use(require('./routes/task'));


app.listen(4000);
console.log('server on port 4000');