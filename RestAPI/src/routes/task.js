const { Router } = require('express');
const router = Router();

const {getTask, getTaskId, createTask, deleteTask, updateTask, getFilteredTasks} = require('../controllers/task.controller')

router.get('/tasks', getTask );
router.get('/tasks/:id', getTaskId );
router.post('/tasks', createTask);
router.delete('/tasks/:id', deleteTask);
router.put('/tasks/:id', updateTask);
router.post('/tasks/filter/:params', getFilteredTasks);


module.exports = router;