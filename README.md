# API Restful para Gestión de Tareas y Colaboradores

Con esta API podemos tener una gestión de tareas y colaboradores, permitiendo la creación, actualización, eliminación, recuperación y filtrado de datos relacionados con estas entidades.

## Propiedades para la coneccion de la base de datos
Se debera importar la siguiente libreria y luego llenar la constante con los parametros adecuados.

```
const { Pool } = require('pg');

const pool = new Pool({
    host: '',
    user: '',
    password: '',
    database: '',
    port: ''
});

```

## Estructura de Objetos JSON

### Respuestas Exitosas

Respuesta de Tareas

```
{
  "success": true,
  "data": [
    {
      "id": 1,
      "description": "Descripción de la tarea",
      "collaborator": "Nombre del colaborador",
      "state": "Estado de la tarea",
      "priority": "Prioridad de la tarea",
      "start_date": "Fecha de inicio",
      "final_date": "Fecha de finalización",
      "notes": "Notas adicionales"
    }
  ],
  "message": "Mensaje informativo (si corresponde)"
}
```

### Respuesta de Colaboradores

```
{
  "success": true,
  "data": [
    {
      "id": 1,
      "name": "Nombre del colaborador",
      // Otros campos de información del colaborador
    }
  ]
}
```

## Estados de Error

### Error
```
{
  "success": false,
  "error": {
    "code": 404,
    "message": "Mensaje de error informativo"
  }
}
```

## Endpoints Disponibles

### Tareas

- GET /tasks: Obtiene todas las tareas.
- GET /tasks/:id: Obtiene una tarea por ID.
- POST /tasks: Crea una nueva tarea.
- DELETE /tasks/:id: Elimina una tarea por ID.
- PUT /tasks/:id: Actualiza una tarea por ID.
- POST /tasks/filter: Filtra tareas según parámetros.

### Colaboradores
- GET /collaborators: Obtiene todos los colaboradores.

## Ejemplo de Uso

### Obtener Todas las Tareas

```
GET /tasks

```
La respuesta será:

 Un resultado con la lista de tareas

 ### Crear una nueva tarea

 ```
 POST /tasks
{
  "description": "Nueva tarea",
  // Datos de la tarea
}
 ```

 La respuesta será:

 Un resultado de confirmación de creación



 ### Obtener colaboradores

```
GET /collaborators

```
Respuesta:

Lista de colaboradores

### Filtrar tareas

```
POST /tasks/filter
{
  "id_collaborator": 1,
  // Otros parámetros de filtrado
}
```

Respuesta:

Lista de tareas filtradas