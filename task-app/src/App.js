import React, {Component } from "react";
import TaskList from "./Components/TaskList";
import "bootstrap/dist/css/bootstrap.min.css";

class App extends Component {

  render() {
    return (
      <div className="App">
        <TaskList />
      </div>
    );
  }
}

export default App;