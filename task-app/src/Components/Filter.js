import React, { Component } from 'react';
import axios from 'axios';


const Collaborators_Url = 'http://localhost:4000/collaborators';
const Filter_URL = 'http://localhost:4000/tasks/filter/:';

class Filters extends Component {
    state = {
        collaborators: '',
        state: '',
        priority: '',
        start_date: '',
        final_date: ''
    };

    /*Method for get all collaborators*/
    getCollaborators = () => {
        axios
            .get(Collaborators_Url)
            .then(response => {
                this.setState({ collaborators: response.data });
            })
            .catch(error => {
                console.log("Error fetching Collaborators:", error);
            });
    }

    /*Function to handle changing values ​​in the filter*/
    handleInputChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        });
    };

    /*Function to filter tasks*/
    handleFilter = () => {
        const { collaborator, state, priority, start_date, final_date } = this.state;
        //console.log('Valores de los filtros En Filter:', { collaborator, state, priority, start_date, final_date });
        const filters = {};
        if (collaborator !== '') {
            filters.id_collaborator = collaborator;
        }
        if (state !== '') {
            filters.state = state;
        }
        if (priority !== '') {
            filters.priority = priority;
        }
        if (start_date !== '' && final_date !== '') {
            filters.start_date = start_date;
            filters.final_date = final_date;
        }

        axios.post(Filter_URL, filters)
            .then(response => {
                //console.log('Respuesta del backend desde el filter:', response.data);
                this.props.updateTasks(response.data);
                console.log(filters);

            })
            .catch(error => {
                console.error('Error to filter task:', error);
            });
    };

    componentDidMount() {
        this.getCollaborators();
    }


    render() {
        
        return (
            <div>
                <div className="filters">
                    <div className="row">
                        <div className="col-md-3">
                            <label htmlFor="collaboratorFilter">Collaborator:</label>
                            <select className="form-control" name="collaborator" id="collaborator" onChange={this.handleInputChange}>
                                <option value="All">Select Collaborator</option>
                                {Array.isArray(this.state.collaborators) && this.state.collaborators.map(collaborator => (
                                    <option key={collaborator.id} value={collaborator.id}>
                                        {collaborator.name}
                                    </option>
                                ))}
                            </select>
                        </div>
                        <div className="col-md-3">
                            <label htmlFor="stateFilter">State:</label>

                            <select className="form-control" name="state" id="st" onChange={this.handleInputChange} >
                                <option value="" defaultValue>Select State</option>

                                <option value="Pending">Pending</option>
                                <option value="In progress">In Progress</option>
                                <option value="Completed">Completed</option>
                            </select>
                        </div>
                        <div className="col-md-3">
                            <label htmlFor="priority">Priority</label>
                            <select className="form-control" name="priority" id="pr" onChange={this.handleInputChange}>
                                <option value="priority" defaultValue>Select Priority</option>
                                <option value="High">High</option>
                                <option value="Medium">Medium</option>
                                <option value="Low">Low</option>
                            </select>
                        </div>
                        <div className="col-md-3">
                            <label htmlFor="dateRangeFilter">Date range:</label>
                            <div className="input-group">
                                <input type="date" className="form-control" name="start_date" id="start_date" onChange={this.handleInputChange} />
                                <div className="input-group-append">
                                    <span className="input-group-text">-</span>
                                </div>
                                <input type="date" className="form-control" name="final_date" id="final_date" onChange={this.handleInputChange} />
                            </div>
                        </div>
                    </div>
                    <div className="row mt-3">
                        <div className="col">
                            <button className="btn btn-primary" onClick={this.handleFilter}>Filter</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Filters;