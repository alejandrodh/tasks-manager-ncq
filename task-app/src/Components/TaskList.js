import React, { Fragment, Component } from 'react';
import axios from 'axios';
import Navbar from "./Navbar";
import Filters from './Filter';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEdit, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { Button, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';

const Collaborators_Url = 'http://localhost:4000/collaborators';
const Tasks_Url = 'http://localhost:4000/Tasks';

class TaskList extends Component {
    state = {
        tasks: [],
        collaborators: [],
        modalAdd: false,
        modalDelete: false,
        showAlert: false,
        form: {
            description: '',
            collaborator: '',
            state: '',
            priority: '',
            start_date: '',
            final_date: '',
            notes: '',
            tipoModal: '',
            collaborator_name: 0

        }
    }

    //HTTP Methods

    /*Method for get all tasks*/
    getTasks = () => {
        axios
            .get(Tasks_Url)
            .then(response => {
                console.log("response data:", response.data);
                this.setState({ tasks: response.data }, () => {
                    console.log("Tasks in state:", this.state.tasks);
                });
            })
            .catch(error => {
                console.log("Error fetching tasks:", error);
            });

    }

    /*Method for send petition "insert" to api*/
    postTasks = async () => {
        const { form } = this.state;

        const taskInProgressOrCompleted = form.state === 'In progress' || form.state === 'Completed';
        console.log("Post: "+form.id_collaborator);
        if (taskInProgressOrCompleted && form.id_collaborator === undefined) {
            this.setState({ showAlert: true });
            return;
        }

        try {
            const response = await axios.post(Tasks_Url, form);
            this.modalAdd();
            this.getTasks();
            console.log(response);
        } catch (error) {
            console.log("Error submitting Task:", error.message);
            this.setState({ errorMessage: 'There was an error submitting the Task.' });
        }
    }

    /*Method for send petition "Update" to api*/
    putTask = async () => {
        const { form } = this.state;

        if (form.state !== 'Pending' && form.state !== 'In progress') {
            console.log("You cannot modify a task that is not 'Pending' or 'In Process'");
            return;
        }

        try {
            const response = await axios.put(Tasks_Url + "/" + form.id, form);
            this.modalAdd();
            this.getTasks();
            console.log(response);
        } catch (error) {
            console.log("Error to Update Task:", error.message);
            this.setState({ errorMessage: 'Error to Update Task.' });
        }
    }

    /*Method for send petition "Delete" to api*/
    deleteTask = async () => {
        const { form } = this.state;

        if (form.state === 'In progress') {
            console.log("Cannot delete a task in 'IN PROGRESS' status");
            return;
        }

        try {
            const response = await axios.delete(Tasks_Url + "/" + form.id);
            this.setState({ modalDelete: false });
            this.getTasks();
            console.log(response);
        } catch (error) {
            console.log("Error to Delete Task:", error.message);
            this.setState({ errorMessage: 'Error to Delete Task.' });
        }
    }

    /*Method for get all collaborators*/
    getCollaborators = () => {
        axios
            .get(Collaborators_Url)
            .then(response => {
                this.setState({ collaborators: response.data });
                console.log("getcollab", response.data);
            })
    }


    //State manipulation and rendering methods
    selectTask = (task) => {
        //const collaboratorId = task.id_collaborator || 0;

        const updatedForm = {
            id: task.id,
            description: task.description,
            id_collaborator: task.id_collaborator,
            collaborator: task.collaborator,
            state: task.state,
            priority: task.priority,
            start_date: this.formatDate(task.start_date),
            final_date: this.formatDate(task.final_date),
            notes: task.notes
        };

        this.setState({
            tipoModal: 'update',
            form: updatedForm
        });
    }

    /*Method for open modal in insert*/
    modalAdd = () => {
        this.setState({ modalAdd: !this.state.modalAdd });
        if (this.state.tipoModal === 'add') {
            this.setState(prevState => ({
                form: {
                    ...prevState.form,
                    collaborator_name: 0,
                    state: 'Pending'
                }
            }));
        }
    }

    updateTasks = (filteredTasks) => {
        this.setState({ tasks: filteredTasks });
    }


    /*Function to handle changing values ​​in the form*/
    handleChange = async e => {
        e.persist();
        const { name, value } = e.target;

        await this.setState(prevState => ({
            form: {
                ...prevState.form,
                [name]: value
            }
        }));
        console.log(this.state.form);
    }

    // Date function
    formatDate = dateString => {
        if (!dateString) return '';

        const date = new Date(dateString);
        const year = date.getFullYear();
        const month = String(date.getMonth() + 1).padStart(2, '0');
        const day = String(date.getDate()).padStart(2, '0');

        return `${year}-${month}-${day}`;
    }

    //rendering
    componentDidMount() {
        this.getTasks();
        this.getCollaborators();
    }

    render() {
        const { form } = this.state;
        let { tasks } = this.state;
        const formatDate = this.formatDate;
        console.log("Form Collaborator ID:", form ? form.collaborator : null);
        if (!Array.isArray(tasks)) {

            return <div>No tasks available</div>;
        }

        return (

            <div className="task">
                <Fragment>
                    <Navbar brand='Task Manager'></Navbar>
                </Fragment>
                <br />
                <button className="btn btn-success" onClick={() => { this.setState({ form: null, tipoModal: 'add' }); this.modalAdd() }}>Add Task</button>
                <br />

                <Filters
                    updateTasks={this.updateTasks}
                    handleFilterRequest={this.handleFilterRequest}
                />

                <br />
                <table className="table">
                    <thead>
                        <tr>
                            <th>Description</th>
                            <th>Collaborator</th>
                            <th>State</th>
                            <th>Priority</th>
                            <th>Start Date</th>
                            <th>Final Date</th>
                            <th>Actions</th>
                        </tr>

                    </thead>
                    <tbody>
                        {tasks.map(task => (
                            <tr key={task.id}>
                                <td>{task.description}</td>
                                <td>{task.collaborator ? task.collaborator : "Not Assigned"}</td>
                                <td>{task.state}</td>
                                <td>{task.priority}</td>
                                <td>{formatDate(task.start_date)}</td>
                                <td>{formatDate(task.final_date)}</td>
                                <td>
                                    <button className="btn btn-primary" onClick={() => { this.selectTask(task); this.modalAdd() }}><FontAwesomeIcon icon={faEdit} />
                                    </button>
                                    {" "}
                                    <button className="btn btn-danger" onClick={() => { this.selectTask(task); this.setState({ modalDelete: true }) }}><FontAwesomeIcon icon={faTrashAlt} />
                                    </button>
                                </td>
                            </tr>

                        ))}
                    </tbody>
                </table>

                <Modal isOpen={this.state.modalAdd}>
                    <ModalHeader style={{ display: 'block' }}>

                        <span style={{ float: 'right' }} onClick={() => this.modalAdd()}>x</span>
                    </ModalHeader>
                    <ModalBody>
                        <div className="form-group">
                            <label htmlFor="description">Description</label>
                            <input className="form-control" type="text" name="description" id="Desc" onChange={this.handleChange} value={form ? form.description : ''} />
                            <br />

                            <label htmlFor="state">Collaborator</label>
                            <select name="id_collaborator" className="form-control" onChange={this.handleChange} value={form ? form.id_collaborator : 0}>

                                <Fragment>

                                    <option value={0}>Select Collaborator</option>
                                    {this.state.collaborators && this.state.collaborators.map(collaborator => (
                                        <option key={collaborator.id} value={collaborator.id}>
                                            {collaborator.name}
                                        </option>
                                    ))}
                                </Fragment>

                            </select>

                            <br />
                            <label htmlFor="state">State</label>
                            <select className="form-control" name="state" id="st" onChange={this.handleChange} value={form ? form.state : 'Pending'}>      
                                <option value="Pending" >Pending</option>
                                <option value="In progress">In Progress</option>
                                <option value="Completed">Completed</option>
                            </select>
                            <br />
                            <br />
                            <label htmlFor="priority">Priority</label>
                            <select className="form-control" name="priority" id="pr" onChange={this.handleChange} value={form ? form.priority : ''}>
                                <option value="">Select Priority</option>
                                <option value="High">High</option>
                                <option value="Medium">Medium</option>
                                <option value="Low">Low</option>
                            </select>
                            <br />
                            <label htmlFor="start_date">Start Date</label>
                            <input className="form-control" type="date" name="start_date" id="start_date" onChange={this.handleChange} value={form ? form.start_date : ''} />
                            <br />
                            <label htmlFor="final_date">Final Date</label>
                            <input className="form-control" type="date" name="final_date" id="final_date" onChange={this.handleChange} value={form ? form.final_date : ''} />
                            <br />
                            <label htmlFor="notes">Notes</label>
                            <input className="form-control" type="text" name="notes" id="notes" onChange={this.handleChange} value={form ? form.notes : ''} />
                        </div>
                    </ModalBody>


                    <ModalFooter className={this.state.tipoModal === 'add' ? 'add or update' : 'Edit Question'}>
                        {this.state.tipoModal === 'add' ? (
                            <div>
                                <button className="btn btn-success" onClick={() => this.postTasks()}>
                                    ADD
                                </button>
                                <button className="btn btn-danger" onClick={() => this.modalAdd()}>Cancel</button>
                            </div>
                        ) : (
                            <>
                                {form && form.state === 'Completed' ? (
                                    <div>
                                        <p>Tasks in 'Completed' state cannot be edited.</p>
                                        <button className='btn btn-secondary' onClick={() => this.setState({ modalAdd: false })}>Close</button>
                                    </div>
                                ) : (
                                    <div>
                                        <button className="btn btn-primary" onClick={() => this.putTask()}>
                                            Update
                                        </button>
                                        <button className="btn btn-danger" onClick={() => this.modalAdd()}>Cancel</button>
                                    </div>
                                )}

                            </>

                        )}
                    </ModalFooter>
                </Modal>

                <Modal className='Select collab' isOpen={this.state.showAlert} toggle={() => this.setState({ showAlert: false })}>
                    <ModalHeader toggle={() => this.setState({ showAlert: false })}>Alert</ModalHeader>
                    <ModalBody>
                        You must select a collaborator to start or finish the task.
                    </ModalBody>
                    <ModalFooter>
                        <Button color="primary" onClick={() => this.setState({ showAlert: false })}>Ok</Button>
                    </ModalFooter>
                </Modal>


                <Modal className='Delete' isOpen={this.state.modalDelete}>
                    <ModalHeader toggle={() => this.setState({ showAlert: false })}>Alert</ModalHeader>
                    <ModalFooter>
                        {form && form.state === 'In progress' ? (
                            <div>
                                <p>Tasks in 'In progress' state cannot be deleted.</p>
                                <button className='btn btn-secondary' onClick={() => this.setState({ modalDelete: false })}>Close</button>
                            </div>
                        ) : (
                            <div>
                                <ModalBody>
                                    You want to delete this task: {form && form.description}
                                </ModalBody>
                                <button className='btn btn-danger' onClick={() => this.deleteTask()}>Yes</button>
                                <button className='btn btn-secondary' onClick={() => this.setState({ modalDelete: false })}>Not</button>
                            </div>
                        )}
                    </ModalFooter>
                </Modal>
            </div>
        );


    }
}

export default TaskList;